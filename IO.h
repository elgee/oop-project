/* IO.h
 * Set of functions and enums to deal with input and output
 * 
 * when the user enters a line this is grabbed and passed to 
 * readCommand which returns true if the user has not input exit
 * and false otherwise. 
 * This calls getCommand on the input stream to decide what to do.
 * Possible commands are defined enumerated in Command and include:
 *  EXIT: exit the proram
 *  HELP: print a help message
 *  CLEAR: unsets a variable.
 *   syntax is "clear <variable name>"
 *  WITH: evaluate an expression with a variable set
 *   syntax is "<expression>, <variable name> = <value>"
 *  EQUALS: set a variable
 *   syntax is "<variable name> = <value>"
 *  EXPRESSION: evaluate an expression
 * 
 * The commands CLEAR, WITH, EQUALS and EXPRESSION have an associated read
 * function which check the syntax and throw badInput exceptions if 
 * there is an error. These are all caught in main.
 * 
 * readExpression reads terms and operations using readTerm and
 * readOperation. The operation enumeration defines the operations 
 * wich may be act between two terms. A term may be an integer, 
 * a rational number or a symbolic variable.
 */
#ifndef IO_H
#define IO_H

#include "Basic.h"
#include "VariableTable.h"

#include <memory>
#include <iostream>

// top level command defined by the syntax of the input
enum class Command
{
    EXIT,
    HELP,
    CLEAR,
    WITH,
    EQUALS,
    EXPRESSION,
};

// mathematical operation within an expression. 
enum class Operation
{
    DIVIDE,
    MULTIPLY,
    SUBTRACT,
    ADD,
};

Operation readOperation(std::istream &);
std::unique_ptr<Basic> doOperation(Basic const *, Basic const *, 
                                   Operation const &);
std::unique_ptr<Basic> readTerm(std::istream &, VariableTable &);
std::unique_ptr<Basic> readExpression(std::string const &, VariableTable &);
std::unique_ptr<Basic> readWith(std::string &, VariableTable &);
std::shared_ptr<Variable> readEquation(std::string &, VariableTable &);
bool readCommand(std::istream &, VariableTable &);
Command getCommand(std::string &);

// messages that are printed to the user are defined in this namespace
namespace IO
{
    std::string const helpMessage
    {
        "This is a symbolic calculator.\n"
        "It only understands integers rational numbers and variables.\n"
        "Use '/' to create a fraction eg. 3/5. Variables must only\n"
        "contain the letters a-z or A-Z.\n"
        "Evaluating an expression:\n"
        "\tType in an expression and it will be evaluated.\n"
        "\tYou can add (+), subtract (-) and multiply (*) e.g:\n"
        "\n"
        "\t[in] x + 3 * 2\n"
        "\t6 + x\n"
        "Setting the value of a variable:\n"
        "\tThe value of a variable may be set using =. The variable\n"
        "\tnow has that value until it is set to another one or\n"
        "\tcleared with the clear command. e.g:\n"
        "\n"
        "\t[in] x = 2\n"
        "\tx = 2\n"
        "\t[in] x * 3\n"
        "\t6\n"
        "\t[in] clear x\n"
        "\t[in] x\n"
        "\tx\n"
        "An expression can be evaluated with a variable set to a\n"
        "given value on the same line using ','. e.g:\n"
        "\n"
        "\t[in] x * 3 / 2, x = 2\n"
        "\t3\n"
        "Other commands:\n"
        "\texit:\texit the program\n"
        "\thelp:\tshow this message\n"
    };
    
    std::string const welcomMessage
    {
        "Welcome to this symbolic calculator.\n"
        "Type help for help. Type exit to exit.\n"
    };
};
#endif
