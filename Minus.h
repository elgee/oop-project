/* Minus.h
 * Class to hold the negative of another which is used to implement
 * negative variables and subtraction as part of a Sum.
 * Holds a copy of a Basic pointer to the object that this represents
 * the negative of.
 * 
 * Constructors:
 *  Can be constructed from a Basic pointer to the type which is to 
 * be taken the negative of. The precedence is set to 2 and the order
 * is the same as that of the term which this is the negative of.
 *  Copy and move constructable from other Minus types
 * 
 * Overloads of the operator+ and operator* are defined for all the 
 * Basic derived types. The implementation of these is templated as
 * private members.
 * 
 * evaluate() returns the result of calling the unitary operator-() 
 * on the type which this is the negative of.
 */
#ifndef MINUS_H
#define MINUS_H

#include "Basic.h"
#include "Expression.h"
#include "Sum.h"
#include "Product.h"
#include "MathInt.h"
#include "MathRational.h"

class Minus : public Expression
{
public:
    Minus() : Expression("-", 2)
    {
    }
    Minus(Basic const *term) : Expression("-", 2)
    {
        m_term = term->copy();
        m_order = term->order();
    }
    Minus(std::unique_ptr<Basic> &&term) : Expression("-", 2)
    {
        m_order = term->order();
        m_term = std::move(term);
    }
    Minus(Minus const &other) : Minus(other.m_term.get())
    {
    }
    Minus(Minus &&other) : Minus()
    {
        swap(*this, other);
    }
        
    Minus& operator=(Minus other)
    {
        swap(*this, other);
        return *this;
    }
    
    // getters and setters
    std::unique_ptr<Basic> const& term() const { return m_term; }
    
    std::unique_ptr<Basic> operator+(Basic const *rhs) const;
    std::unique_ptr<Basic> operator+(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator+(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator+(Sum const &rhs) const;
    std::unique_ptr<Basic> operator+(Product const &rhs) const;
    std::unique_ptr<Basic> operator+(Minus const &rhs) const;
    std::unique_ptr<Basic> operator+(VariablePtr const &rhs) const;
    
    std::unique_ptr<Basic> operator*(Basic const *rhs) const;
    std::unique_ptr<Basic> operator*(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator*(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator*(Sum const &rhs) const;
    std::unique_ptr<Basic> operator*(Product const &rhs) const;
    std::unique_ptr<Basic> operator*(Minus const &rhs) const;
    std::unique_ptr<Basic> operator*(VariablePtr const &rhs) const;
    
    // return a Basic pointer to a copy of the term which this is the 
    // negative of
    std::unique_ptr<Basic> operator-() const;
    
    std::unique_ptr<Basic> evaluate();
    char const* type() const { return "Minus"; }
    void print(std::ostream &os) const;
    std::unique_ptr<Basic> copy() const;
    
    friend void swap(Minus &first, Minus &second);
private:
    // these define the implementation of adding and multiplying
    // by basic derived types
    template <typename T> 
    std::unique_ptr<Basic> add(T const &rhs) const;
    template <typename T> 
    std::unique_ptr<Basic> times(T const &rhs) const;
    
    // the term which this is the negative of.
    std::unique_ptr<Basic> m_term;
};

#endif
