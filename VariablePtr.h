/* VariablePtr.h
 * This object points to a variable in the variable table and defines
 * the mathematical overloads that are required of a Basic derived type
 * There may be many instances of VariablePtr that point to the same 
 * variable. Every VariablePtr which represents the same variable should
 * point to the same instance of that variable which is managed by the 
 * variable table.
 * 
 * order = 1 if unset or order of value if set
 * precedence = 4
 * 
 * Constructable from a shared pointer to a variable. This is typically
 * returned from the variable table. 
 * Also copy and move constructable.
 * 
 * default implementations of mathematical operations are defined in 
 * private templated member functions
 * 
 * Addition: returns a Basic pointer to a Sum with this and the rhs as 
 * terms
 * 
 * Multiplication: returns a Basic pointer to a Product with this and 
 * rhs as terms
 * 
 * evaluate() returns a copy if the variable is unset or the value to
 * which the variable is set if it is.
 */
#ifndef VARIABLE_PTR_H
#define VARIABLE_PTR_H

#include "Variable.h"
#include "Basic.h"
#include "MathInt.h"
#include "MathRational.h"
#include "Expression.h"
#include "Sum.h"
#include "Product.h"
#include "Rational.h"
#include <string>
#include <utility>
#include <memory>

class VariablePtr : public Basic 
{
public:
    VariablePtr();
    VariablePtr(VariablePtr const &other);
    VariablePtr(VariablePtr &&other);
    VariablePtr(std::shared_ptr<Variable> const &ptr);
    VariablePtr& operator=(VariablePtr other);
    
    friend void swap(VariablePtr &first, VariablePtr &second);
    
    std::unique_ptr<Basic> operator+(Basic const *rhs) const;
    std::unique_ptr<Basic> operator+(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator+(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator+(Sum const &rhs) const;
    std::unique_ptr<Basic> operator+(Product const &rhs) const;
    std::unique_ptr<Basic> operator+(Minus const &rhs) const;
    std::unique_ptr<Basic> operator+(VariablePtr const &rhs) const;
    std::unique_ptr<Basic> operator*(Basic const *rhs) const;
    std::unique_ptr<Basic> operator*(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator*(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator*(Sum const &rhs) const;
    std::unique_ptr<Basic> operator*(Product const &rhs) const;
    std::unique_ptr<Basic> operator*(Minus const &rhs) const;
    std::unique_ptr<Basic> operator*(VariablePtr const &rhs) const;
    
    std::unique_ptr<Basic> operator-() const;
    
    char const* type() const { return "Variable"; }
    void print(std::ostream &os) const;
    // returns a copy if the variable is not set and the value that 
    // the variable represents if it is
    std::unique_ptr<Basic> evaluate();
    // const version since evaluating a variable just returns a copy
    // and this is necessary in other const functions
    std::unique_ptr<Basic> evaluate() const;
	std::unique_ptr<Basic> copy() const;
    // set the variable which this holds
    void set(Basic const *rhs);
    
private:
    // pointer to a variable which is held in the variable table
    std::shared_ptr<Variable> m_ptr;
    
    template <typename T> 
    std::unique_ptr<Basic> add(T const &rhs) const;
    template <typename T> 
    std::unique_ptr<Basic> times(T const &rhs) const;
};

#endif
