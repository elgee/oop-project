/* VariableTable.h
 * This is a map of variable name to a shared pointer to the Variable
 * which holds the information about each variable. This class 
 * manages all the variables. The VariablePtr class generates a
 * shared pointer to the variable from this table.
 * 
 * variables may be created and added to the map by passing a string
 * which represents the name of the variable or a variable itself to 
 * createVariable
 * 
 * destroyVariable(string const &) removes the variable from the map
 *  Throws variableInUse exception if anything is pointing to the 
 *  variable
 * 
 * exists(string const &) returns true if the variable is in the map
 *  and false otherwise. 
 * 
 * operator[] is overloaded so that indexing with a variable name 
 * returns a shared pointer to the variable which is used to 
 * instantiate VariablePtr objects. Throws runtime_error if variable
 * doesn't exist.
 */
#ifndef VARIABLE_TABLE_H
#define VARIABLE_TABLE_H

#include "Variable.h"
#include <string>
#include <map>
#include <memory>

class VariableTable
{
public:
    void createVariable(std::string const &name);
    void createVariable(Variable const &variable);
    void createVariable(Variable &&variable);
    void destroyVariable(std::string const &name);
    bool exists(std::string const &name);
    std::shared_ptr<Variable> operator[](std::string const &name);
private:
    std::map<std::string, std::shared_ptr<Variable>> m_table;
};

#endif
