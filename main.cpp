/* main.cpp
 * main loop for the program. Runs readCommand (see IO.h) while the
 * user doesn't exit the program. Any exceptions are caught here, the
 * error is printed and then the user can enter another expression.
 */
#include "Basic.h"
#include "Rational.h"
#include "MathInt.h"
#include "MathRational.h"
#include "Sum.h"
#include "Product.h"
#include "Minus.h"
#include "Variable.h"
#include "VariablePtr.h"
#include "VariableTable.h"
#include "Exceptions.h"
#include "IO.h"

#include <iostream>
#include <utility>
#include <list>
#include <forward_list>
#include <algorithm>
#include <string>
#include <sstream>
#include <iterator>

/* TODO
 * Need to check equation for varaibles on the right hand side that are
 * the same as on the left.
 * Rational number input with a minus
 */

int main() 
{
    VariableTable vars;
    bool keepRunning{ true };
    std::cout << IO::welcomMessage;
    while (keepRunning)
    {
        try
        {
            std::cout << "[in] ";
            keepRunning = readCommand(std::cin, vars);
        }
        catch (std::runtime_error &error)
        {
			std::cerr << error.what() << '\n';
        }
    }
    return 0;
}