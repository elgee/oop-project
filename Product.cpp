/* Product.cpp
 * Operations are implemented using default templated functions for
 * adding and multiplying with specialisations where required. 
 * 
 * Multiplication is done through the overloaded operator*= in which 
 * the default implementation appends the argument to the list of terms
 * and sets m_sorted to false. A specialisation exists for an argument
 * of type Product which merges the terms.
 * 
 * The default implementation of adding is to return a Sum which has
 * this and the rhs as terms. In the case of adding to a Sum, Sum's 
 * implementation is used.
 * 
 * evaluate() iterates through the terms and multiplies them together
 * whilst recursively calling evaluate so that nested expressions are 
 * evaluated.
 */

#include "Basic.h"
#include "Product.h"
#include "MathInt.h"
#include "MathRational.h"
#include "Sum.h"
#include "Exceptions.h"

using std::unique_ptr;
using std::shared_ptr;
using std::forward_list;

// default constructor
Product::Product() : Associative("*", 2)
{
}

// construct from a copy of a list of terms
Product::Product(forward_list<shared_ptr<Basic>> const &terms) 
    : Associative("*", 2, terms)
{
    for (auto it = terms.begin(); it != terms.end(); ++it)
        m_order += (*it)->order();
}

// construct by moveing the list of terms into this class
Product::Product(forward_list<shared_ptr<Basic>> &&terms)
    : Associative("*", 2, std::move(terms))
{
    for (auto it = terms.begin(); it != terms.end(); ++it)
        m_order += (*it)->order();
}

// copy constructor
Product::Product(Product const &other) 
    : Associative(other.m_symbol, 2, other.m_terms)
{
   m_sorted = other.m_sorted;
   m_order = other.m_order;
}

// move constructor
Product::Product(Product &&other) : Product()
{
    swap(*this, other);
}

// copy/move assignment
Product& Product::operator=(Product other)
{
    swap(*this, other);
    return *this;
}

// multiplies the terms together
unique_ptr<Basic> Product::evaluate() 
{
	if (m_terms.empty()) throw NoTermsError();
	// copy fist term
    if (!m_sorted) sort();
    unique_ptr<Basic> result{ m_terms.front()->evaluate() };
    // iterate through rest recursively calling
	for (auto it = ++m_terms.begin(); it != m_terms.end(); ++it)
		result = *result * (*it)->evaluate().get();
		
	return result;
}

// returns a Basic pointer to a copy of this
unique_ptr<Basic> Product::copy() const
{
    return unique_ptr<Basic>{ new Product{ *this } };
}


// template type for appending Basic derived types to a product. 
// the copy() function must be defined (which is necessary for Basic 
// derived types) and should return a unique_ptr<Basic> which points to 
// a copy of rhs.
template <typename T>
Product& Product::operator*=(T const &rhs)
{
    m_terms.push_front(shared_ptr<Basic>{ rhs.copy() });
    m_sorted = false;
    return *this;
}

// specialisation for product merges lists
template <> Product& Product::operator*=(Product const &rhs)
{
    // local copy since merging empties rhs
    Product tmp{ rhs };
	if (!m_sorted) sort();
    if (!tmp.m_sorted) tmp.sort();
	//rhs.m_terms.sort();
	//m_terms.sort();
	m_terms.merge(tmp.m_terms, 
		[&](shared_ptr<Basic> const &first, shared_ptr<Basic> const &second)
	{
		return first->order() < second->order();
	});
    m_order += rhs.m_order;
    return *this;
}

// member functions to return Basic pointer
unique_ptr<Basic> Product::operator+(Basic const *rhs) const
{
    return *rhs + *this;
}
unique_ptr<Basic> Product::operator*(Basic const *rhs) const
{
    return *rhs * *this;
}

// default implementation of adding, creating a Sum which has this
// and the rhs as terms
template<typename T>
unique_ptr<Basic> Product::add(T const &rhs) const
{
    forward_list<shared_ptr<Basic>> terms;
    terms.push_front(shared_ptr<Basic>{ new Product{ *this } });
    Sum result{ terms };
    result += rhs;
    return unique_ptr<Basic>{ new Sum{ std::move(result) } };
}

// default implementation of multiplying which uses the compound
// assignment operator *= on the rhs
template<typename T>
unique_ptr<Basic> Product::times(T const &rhs) const
{
    Product result{ *this };
    result *= rhs;
    return unique_ptr<Basic>{ new Product{ std::move(result) } };
}

unique_ptr<Basic> Product::operator+(MathInt const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> Product::operator+(MathRational const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Product::operator+(Product const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Product::operator+(Minus const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Product::operator+(VariablePtr const &rhs) const
{
    return this->add(rhs);
}

// use Sum's definition
unique_ptr<Basic> Product::operator+(Sum const &rhs) const
{
    return rhs + *this;
}

unique_ptr<Basic> Product::operator*(MathInt const &rhs) const
{ 
    return this->times(rhs);
}

unique_ptr<Basic> Product::operator*(MathRational const &rhs) const
{ 
    return this->times(rhs);
}

// use sum's definition
unique_ptr<Basic> Product::operator*(Sum const &rhs) const
{ 
    return rhs * *this;
}

unique_ptr<Basic> Product::operator*(Product const &rhs) const
{ 
    return this->times(rhs);
}

unique_ptr<Basic> Product::operator*(Minus const &rhs) const
{ 
    return this->times(rhs);
}

unique_ptr<Basic> Product::operator*(VariablePtr const &rhs) const
{ 
    return this->times(rhs);
}

//other non member functions
void swap(Product &first, Product &second)
{
    std::swap(first.m_terms, second.m_terms);
    std::swap(first.m_sorted, second.m_sorted);
    std::swap(first.m_order, second.m_order);
}
