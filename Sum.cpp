/* Sum.cpp
 * Default implementations of multiplication and addition are defined
 * in private member functions. These use the operator+= and 
 * operator*= overloads. 
 * 
 * Multiplication: 
 *  by default this multiplies every term in the sum by the rhs
 * This is specialised for multiplying by sums to evaluate all cross
 * terms.
 * 
 * Addition:
 *  by default this creates a copy of this and appends the rhs to the
 * terms. This is specialised for adding another sum which merges the 
 * terms in the two sums.
 * 
 * evaluate() iterates through all the terms adding them together 
 * after recursively calling evaluate on them.
 * 
 * print(): the default implementation of Associative is overriden to
 * check for negative numbers so that the "+" symbol can be replaced
 * by "-"
 * 
 */
#include "Sum.h"
#include "MathInt.h"
#include "MathRational.h"
#include "Product.h"
#include "VariablePtr.h"
#include "Exceptions.h"
#include <memory>
#include <sstream>
#include <cstring>

using std::unique_ptr;
using std::shared_ptr;

// constructors
Sum::Sum(std::forward_list<shared_ptr<Basic>> const &terms) 
    : Associative("+", 1, terms)
{
}

Sum::Sum(std::forward_list<shared_ptr<Basic>> &&terms)
    : Associative("+", 1, std::move(terms))
{
}
    
Sum::Sum(Sum const &other) 
    : Associative(other.m_symbol, 1, other.m_terms)
{
    m_sorted = other.m_sorted;
}

Sum::Sum(Sum &&other) : Sum()
{
    swap(*this, other);
}

// assignment operator for expressions
Sum& Sum::operator=(Sum other)
{
    swap(*this, other);
    return *this;
}

// template type for appending Basic derived types to a Sum. the copy()
// function must be defined (which is necessary for Basic derived types 
// and should return a unique_ptr<Basic> which points to a copy of rhs.
template <typename T>
Sum& Sum::operator+=(T const &rhs)
{
        m_terms.push_front(shared_ptr<Basic>{ rhs.copy() });
        m_sorted = false;
        return *this;
}

// specialisation for sum since the terms need combining
template<> Sum& Sum::operator+=(Sum const &rhs)
{
    // local copy of rhs  since rhs.m_terms emptied by merge
    Sum tmp{ rhs };
    if (!m_sorted) sort();
    if (!tmp.m_sorted) tmp.sort();
    m_terms.merge(tmp.m_terms,
		[&](shared_ptr<Basic> const &first, shared_ptr<Basic> const &second)
	{
		return first->order() < second->order();
	});
    return *this;
}

// loop through each element multiply by rhs which is evaluated by the
// most derived overload of the Basic object that *it points to 
template <typename T> Sum& Sum::operator*=(T const &rhs)
{
	if (m_terms.empty()) throw NoTermsError();
    for (auto it = m_terms.begin(); it != m_terms.end(); ++it)
    {
        (*it)->order() += rhs.order();
        *it = **it * rhs;
    }

    m_sorted = false;
    return *this;
}

    
// specialisation for sum since the terms need merging
template <> Sum& Sum::operator*=(Sum const &rhs)
{
    // create a new vector of terms by multiplying the terms from each
    // sum in turn then replace this with result.
	if (m_terms.empty() || rhs.m_terms.empty()) throw NoTermsError();
    Sum result;
    for (auto it1 = m_terms.begin(); it1 != m_terms.end(); ++it1)
    {
        for (auto it2 = rhs.m_terms.begin(); it2!= rhs.m_terms.end(); ++it2)
            result.m_terms.push_front(**it1 * it2->get());
    }
    std::swap(result.m_terms, m_terms);
    // no longer sorted
    m_sorted = false;
    return *this;
}

// loops through the terms adding them together and recursively 
// evaluating them
unique_ptr<Basic> Sum::evaluate() 
{
	if (m_terms.empty()) throw NoTermsError();
    if (!m_sorted) sort();
	// copy first term
	unique_ptr<Basic> result{ m_terms.front()->evaluate() };
	//iterate through rest recursively calling evaluate
	for (auto it = ++m_terms.begin(); it != m_terms.end(); ++it)
	{
		result = *result + (*it)->evaluate().get();
		m_sorted = false;
	}

	return result;
}

unique_ptr<Basic> Sum::copy() const
{
    return unique_ptr<Basic>{ new Sum{ *this } };
}

// member functions returning Basic pointers
unique_ptr<Basic> Sum::operator+(Basic const *rhs) const
{
    return *rhs + *this;
}

unique_ptr<Basic> Sum::operator*(Basic const *rhs) const
{
    return *rhs * *this;
}

// contains the implementation for all the operator+ and operator*
// have to make temporary copies here to utilise +=,*= operators.
// The result then moved to the new memory 
template <typename T>
unique_ptr<Basic> Sum::add(T const &rhs) const
{
    Sum result{ *this };
    return unique_ptr<Basic>{ new Sum{ std::move(result += rhs ) } };
}

template <typename T>
unique_ptr<Basic> Sum::times(T const &rhs) const
{
    Sum result{ *this };
    return unique_ptr<Basic>{ new Sum{ std::move(result *= rhs ) } };
}

unique_ptr<Basic> Sum::operator+(Sum const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Sum::operator+(MathInt const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Sum::operator+(MathRational const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Sum::operator+(Product const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Sum::operator+(Minus const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Sum::operator+(VariablePtr const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Sum::operator*(MathInt const &rhs) const
{
    return this->times(rhs);
}

unique_ptr<Basic> Sum::operator*(MathRational const &rhs) const
{
    return this->times(rhs);
}

unique_ptr<Basic> Sum::operator*(Sum const &rhs) const
{
    return this->times(rhs);
}

unique_ptr<Basic> Sum::operator*(Product const &rhs) const
{
    return this->times(rhs);
}

unique_ptr<Basic> Sum::operator*(Minus const &rhs) const
{
    return this->add(rhs);
}

unique_ptr<Basic> Sum::operator*(VariablePtr const &rhs) const
{
    return this->times(rhs);
}

void Sum::print(std::ostream &os) const
{
    // use to check output for minus signs before printing
    std::stringstream output;
    // use to loop over elements and not print symbol at end
    int size{ static_cast<int>( 
        std::distance(m_terms.begin(), m_terms.end())) };
        
    // don't if empty, loop never executes
    if (size!=0) os << *m_terms.front();
    
    auto it = ++m_terms.begin();
    for (int element_count{ 1 }; element_count < size; ++element_count)
    {
        output << **it;
        if (output.peek() == '-')
        {
            std::string ostr = output.str();
            ostr.insert(1,1,' ');
            os << " " << ostr;
        }
        else
        {
            os << " " << m_symbol << " ";
            os << **it;
        }
        ++it;
        // clear stringstream
        output.str("");
    }
}
            
        
        
// other non member functions
void swap(Sum &first, Sum &second)
{
    std::swap(first.m_terms, second.m_terms);
    std::swap(first.m_sorted, second.m_sorted);
}
