// Variable.cpp

#include "Variable.h"

// ges a variable name from input stream which may be a sequence of 
// uppercase and or lowercase letters. Sets failbit of is on failure
std::istream& operator>>(std::istream& is, Variable &variable)
{
    char next;
    std::string variableName;
    while(true)
    {
        next = is.peek();
        if (next >= 'a' && next <='z' || next >= 'A' && next <= 'Z')
        {
            variableName.append(1, next);
            is.ignore();
        }
        else break;
    }
    
    if (variableName.length() == 0) is.setstate(std::ios_base::failbit);
    else variable.name(variableName);
    
    return is;
}
