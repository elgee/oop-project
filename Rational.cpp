#include "Rational.h"
#include <stdexcept>
#include <utility>
#include <iostream>

// constructors
Rational::Rational(int numerator, int denominator)
    : m_numerator{ numerator }, m_denominator{ denominator }
{
    if (denominator == 0)
        throw std::runtime_error{ "Attempt to create fraction with "
            "zero denominator.\n" };
}

Rational::Rational(Rational &&other) : Rational()
{
    swap(*this, other);
}

Rational& Rational::operator=(Rational other)
{
    swap(*this, other);
    return *this;
}

Rational Rational::operator-() const
{
    return Rational{ -m_numerator, m_denominator };
}
// compound assignment overloads
Rational& Rational::operator+=(Rational const &rhs)
{
    m_numerator = m_numerator * rhs.m_denominator +
                  rhs.m_numerator * m_denominator;
    m_denominator *= rhs.m_denominator;
    simplify();
    return *this;
}

Rational& Rational::operator+=(int const rhs)
{
    return *this += Rational{ rhs, 1 };
}

Rational& Rational::operator-=(Rational const &rhs)
{
    m_numerator = m_numerator * rhs.m_denominator -
                  rhs.m_numerator * m_denominator;
    m_denominator *= rhs.m_denominator;
    simplify();
    return *this;
}

Rational& Rational::operator-=(int const rhs)
{
    return *this -= Rational{ rhs, 1 };
}

Rational& Rational::operator*=(Rational const &rhs)
{
    m_numerator *= rhs.m_numerator;
    m_denominator *= rhs.m_denominator;
    simplify();
    return *this;
}

Rational& Rational::operator*=(int const rhs)
{
    return *this *= Rational{ rhs, 1 };
}

Rational& Rational::operator/=(Rational const &rhs)
{
    m_numerator *= rhs.m_denominator;
    m_denominator *= rhs.m_numerator;
    simplify();
    return *this;
}

Rational& Rational::operator/=(int const rhs)
{
    return *this /= Rational{ rhs, 1 };
}

// divides by the greatest common divider
Rational& Rational::simplify()
{
    int divisor{ gcd(m_numerator, m_denominator) };
    m_numerator /= divisor;
    m_denominator /= divisor;
    return *this;
}

// non-member functions
// binary mathematical operations
Rational operator+(Rational lhs, Rational const &rhs)
{
    lhs += rhs;
    return lhs;
}

Rational operator+(Rational lhs, int const rhs)
{
    lhs += rhs;
    return lhs;
}

Rational operator+(int const lhs, Rational const &rhs)
{
    return rhs + lhs;
}

Rational operator-(Rational lhs, Rational const &rhs)
{
    lhs -= rhs;
    return lhs;
}

Rational operator-(Rational lhs, int const rhs)
{
    lhs -= rhs;
    return lhs;
}
Rational operator-(int const lhs, Rational const &rhs)
{
    return rhs - lhs;
}

Rational operator*(Rational lhs, Rational const &rhs)
{
    lhs *= rhs;
    return lhs;
}

Rational operator*(Rational lhs, int const rhs)
{
    lhs *= rhs;
    return lhs;
}
Rational operator*(int const lhs, Rational const &rhs)
{
    return rhs * lhs;
}

Rational operator/(Rational lhs, Rational const &rhs)
{
    lhs /= rhs;
    return lhs;
}

Rational operator/(Rational lhs, int const rhs)
{
    lhs /= rhs;
    return lhs;
}

Rational operator/(int const lhs, Rational const &rhs)
{
    return rhs / lhs;
}
// useful functions
void swap(Rational &first, Rational &second)
{
    std::swap(first.m_numerator, second.m_numerator);
    std::swap(first.m_denominator, second.m_denominator);
}

std::ostream& operator<<(std::ostream &os, Rational const &a)
{
    if (a.m_numerator * a.m_denominator < 0) os << "-";
    os << abs(a.m_numerator) << "/" << abs(a.m_denominator);
    return os;
}

// extracts a rational number from the input stream assuming that
// it is two integers seperated by a '/'. If this extraction fails
// the input stream's failbit is set and rational is left untouched.
std::istream& operator>>(std::istream &is, Rational &rational)
{
    int numerator;
    is >> numerator;
    
    if (is.fail()) return is;
    
    char slash;
    if (is.peek() == '\n')
    {
        is.setstate(std::ios_base::failbit);
        return is;
    }
    else is >> slash;
    
    if (slash != '/')
    {
        is.putback(slash);
        is.putback(numerator);
        is.setstate(std::ios_base::failbit);
        return is;
    }
    
    int denominator;
    if (is.peek() == '\n') 
    {
        is.putback(slash);
        is.putback(numerator);
        is.setstate(std::ios_base::failbit);
        return is;
    }
    else is >> denominator;
    
    if (is.fail())
    {
        is.putback(slash);
        is.putback(numerator);
        is.setstate(std::ios_base::failbit);
        return is;
    }
    
    // extraction succesful
    rational = Rational{ numerator, denominator };
    return is;
}

