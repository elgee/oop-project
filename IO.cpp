// IO.cpp 
#include "IO.h"
#include "MathInt.h"
#include "MathRational.h"
#include "VariablePtr.h"
#include "Exceptions.h"

#include <utility>
#include <list>
#include <string>
#include <sstream>
#include <cstring>
#include <limits>

using std::unique_ptr;
using std::shared_ptr;

// read an operation from an input stream and return the associated
// enumerated type. Throw BadInput exception if not understood`
Operation readOperation(std::istream &is)
{
	is >> std::ws;
    char ch = is.get();
    switch (ch)
    {
        case '/':
            return Operation::DIVIDE;
            break;
        case '*': 
            return Operation::MULTIPLY;
            break;
        case '-':
            return Operation::SUBTRACT;
            break;
        case '+':
            return Operation::ADD;
            break;
        
        default:
            throw BadInput{"Error: operation not understood."};
    };
}

// execute the operation described by op on lhs and rhs
unique_ptr<Basic> doOperation(Basic const *lhs, Basic const *rhs, 
                              Operation const &op)
{
    switch(op)
    {
        // divide only implemented to create rational numbers. 
        // this can be changed once powers introduced
        // throws bad input if lhs and rhs are not integers
        case Operation::DIVIDE:
        {
            if (std::strcmp(lhs->type(), "Int") != 0 ||
                    std::strcmp(rhs->type(), "Int") != 0)
                throw BadInput{ "Error: / must have integers as "
                    "arguments" };
        
            Rational result;
            MathInt const* 
            numeratorPtr{ dynamic_cast<MathInt const*>(lhs) };
            MathInt const*
            denominatorPtr{ dynamic_cast<MathInt const*>(rhs) };
            result = Rational{ numeratorPtr->value(), 
                                denominatorPtr->value() };
            result.simplify();
            return unique_ptr<Basic>{ 
                new MathRational{ std::move(result) } };
        }        
        case Operation::MULTIPLY:
            return *lhs * rhs;
        case Operation::SUBTRACT:
            return *lhs + Minus{ rhs };
        case Operation::ADD:
            return *lhs + rhs;
        default:
            throw std::runtime_error{ "Error: invalid operation "
                "passied to doOperation" };
    }
}

// read in a term from input stream. This may be an integer, rational 
// number or a symbolic variable. Throws BadInput if not 
unique_ptr<Basic> readTerm(std::istream &is, VariableTable &variables)
{
    // attempt to extract integer
    int i;
	is >> std::ws;
    is >> i;
    if (is.fail()) is.clear();
    else return unique_ptr<Basic>{ new MathInt{ i } };
    
    // atempt to extract variable and add it to the variable table 
    // if it isn't already there
    Variable v;
    is >> v;
    if (is.fail()) is.clear();
    else
    {
        if (variables.exists(v.name()) == 0)
            variables.createVariable(v.name());
            
        return unique_ptr<Basic>{ 
            new VariablePtr{ variables[v.name()] } };
    }
    
    throw BadInput{"Error: input type not understood."};
}
    
// extract an expression from the input stream with known variables
// in variable table. Extractions may throw BadInput as well as 
// unmatched brackets
unique_ptr<Basic> 
readExpression(std::string const &str, VariableTable &variables)
{
    // get input string and store in a stream to attempt extraction
    std::stringstream input{ str };
    
    // lists to store variables and operators
    std::list<unique_ptr<Basic>> terms;
    std::list<Operation> ops;
    
    input >> std::ws;
    // check for opening bracket else read firs term then loop over
    // the other terms and operators
    enum class Input 
    {
        OPERATION,
        TERM,
    };
    
    // used for counting brackets
    static int recurssionLevel{ 0 };
    // used for tracking position in input when recurssing
	static std::streampos inputPos{ 0 };
	//static long int inputPos{ 0 };
    // holds the last input type. OPERATION starts the loop in the 
    // correct place
    Input lastInput { Input::OPERATION };
    
    while (!input.eof())
    {
        switch (lastInput)
        {
            case Input::OPERATION:
            {
                // if the term is negative then it will be read in
                // as a Minus type
                bool negative{ false };
                if (input.peek() == '-')
                {
                    negative = true;
                    input.ignore();
                    input >> std::ws;
                }
                
                // reading in a bracket calls the function recursively
                // on the rest of the string after the bracket. This 
                // function returns when a matching bracket at the same
                // level is reached
                if (input.peek() == '(')
                {
                    recurssionLevel += 1;
                    input.ignore();
                    input >> std::ws;
					// grab the rest of the input from the bracket and then put input
                    // back to where it was
                    std::streampos posAfterOpen = input.tellg();
					std::string fromBracket;
                    getline(input, fromBracket);
                    input.seekg(posAfterOpen);
                    
                    // recurse with substring
					if (negative)
						terms.push_front(unique_ptr < Basic > {
						new Minus { readExpression(fromBracket, 
							variables) } });
					else
						terms.push_front(
							readExpression(fromBracket, variables) );
					
                    // put input stream to after closing bracket
					input.seekg(inputPos, std::ios_base::cur);
					inputPos = 0;
					input >> std::ws;
					lastInput = Input::TERM;
                }
                else
                {
                    // try read a term throws BadInput on error.
                    if (negative)
                        terms.push_front(unique_ptr<Basic>{ 
                            new Minus { readTerm(input, variables)} } );
                    else
                        terms.push_front(readTerm(input, variables));
                    
                    input >> std::ws;
                    lastInput = Input::TERM;
                }
                break;
            }
            case Input::TERM:
                if (input.peek() == ')')
                {
                    if (recurssionLevel == 0)
                        throw BadInput{ "Error: unexpected ')'" };
                    
                    recurssionLevel -= 1;
                    input.ignore();
                    inputPos = input.tellg();
                    
                    input.ignore(
                        std::numeric_limits<std::streamsize>::max(),
                        '\n');
                }
                else
                {
                    ops.push_front(readOperation(input));
                    input >> std::ws;
                    lastInput = Input::OPERATION;
                }
        }
    }
    
    terms.reverse();
    ops.reverse();
    auto termIt = terms.begin();
    auto opIt = ops.begin();
    // combine adjacent terms with relevant operato
    for (int opIndex{ 0 };
         opIndex <= static_cast<int>(Operation::ADD); 
         ++opIndex)
    {
        while (opIt != ops.end())
        {
            if (*opIt == static_cast<Operation>(opIndex))
            {
                unique_ptr<Basic> result{ doOperation(termIt->get(), 
                    std::next(termIt)->get(), 
                    static_cast<Operation>(opIndex)) };
                *termIt = std::move(result);
                terms.erase(std::next(termIt));
                // delete current op and move to next
                ops.erase(opIt++);
            }
            else
            {
                ++opIt;
                ++termIt;
            }
        }
        // reset iterators
        opIt = ops.begin();
        termIt = terms.begin();
    }
        
    return std::move(terms.front());
}

// Read an equation from string which expects a variable on the lhs of
// an equals sign with an expression or value on the rhs, throws
// BadInput if fails
shared_ptr<Variable>
readEquation(std::string &str, VariableTable &variables)
{
    size_t pos{ str.find("=") };
    std::stringstream varStream{ str.substr(0, pos) };
    Variable v;
    varStream >> std::ws;
    varStream >> v;
    varStream >> std::ws;
    // if the extraction failed or there is anything else in 
    // the stream error
    if (varStream.fail() || !varStream.eof()) 
        throw BadInput{ "Error: = must have variale on left."};
    
    std::string exprString{ str.substr(pos+1, str.length() - pos -1) };
    
    // check for self assignment 
    if (exprString.find(v.name()) != std::string::npos)
        throw BadInput{ "Error: Can't assign variable to expression "
            "containing itself" };
            
    // throws BadInput if fails
    unique_ptr<Basic>
    expression{ readExpression(exprString, variables) };
    
    
    if (variables.exists(v.name()))
        variables[v.name()]->set(*expression);
    else
    {
        v.set(*expression);
        variables.createVariable(v);
    }
    return variables[v.name()];
}    

// reads a with command which evaluates and expression with a variable
// set to a value. If evaluating the expression fails the variable is
// reset. Expects an expression on the lhs of a comma and an equation 
// on the rhs
unique_ptr<Basic>
readWith(std::string &str, VariableTable &variables)
{
    size_t pos{ str.find(",") };
    std::string exprString{ str.substr(0, pos) };
    std::string eqString{ str.substr(pos+ 1, str.length() - pos - 1) };
        
    // this sets the variable in the equation to the value 
    // in equation and throws BadInput if fails.
    shared_ptr<Variable> 
    v_ptr{ readEquation(eqString, variables) };
    
    unique_ptr<Basic> expr;
    // if there is bad input the variable needs reseting
    try
    {
        expr = readExpression(exprString, variables);
    }
    catch (BadInput &)
    {
        v_ptr->reset();
        throw;
    }
    
    return expr;
}

// reads the clear command which clears a variable. Expects
// "clear" followed by a varible name which is in the variable table
// throws BadInput if fails
void readClear(std::string input, VariableTable &variables)
{
    // remove "clear" command from is
    std::stringstream is{ input };
    is.ignore(5);
    std::string varName;
    if (is >> varName)
    {
        if (variables.exists(varName))
            variables[varName]->reset();
        else
            throw BadInput{ "Error: Variable does not exist"};
    }
    else
    {
        is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        is.clear();
        throw BadInput{ "Error: clear can only be used with known"
            "variables"};
    }
}
    
        
// gets the command type from a string default is to attempt to
// evaluate an expression
Command getCommand(std::string &input)
{
    if (input == "exit")
        return Command::EXIT;
    else if (input.substr(0,5) == "clear")
        return Command::CLEAR;
    else if (input.substr(0,4) == "help")
        return Command::HELP;
    else if (input.find(",") != std::string::npos)
        return Command::WITH;
    else if (input.find("=") != std::string::npos)
        return Command::EQUALS;
    else
        return Command::EXPRESSION;
}
    
// interface between user and funcions. Return value tells the caller
// whether to run again. True is returned unless exit is called
bool readCommand(std::istream &is, VariableTable &variables)
{
    // get the command into a string or parsing
    std::string inputString;
    std::getline(is, inputString);
    
    Command command{ getCommand(inputString) };

    // execute command and print output
    switch (command)
    {
        case Command::EXIT:
            return false;
        case Command::HELP:
            std::cout << IO::helpMessage;
            break;
        case Command::CLEAR:
            readClear(inputString, variables);
            break;
        case Command::WITH:
        {
            unique_ptr<Basic>
            expr{ readWith(inputString, variables) };
            std::cout << *expr->evaluate() << '\n';
            break;
        }
        case Command::EQUALS:
        {
            shared_ptr<Variable>
            v_ptr{ readEquation(inputString, variables) };
            std::cout << v_ptr->name() << " = " << 
                            *v_ptr->value() << '\n';
            break;
        }
        case Command::EXPRESSION:
            unique_ptr<Basic> 
            expr{ readExpression(inputString, variables) };
            std::cout << *expr->evaluate() << '\n';
    }    
    
    return true;
}
    
