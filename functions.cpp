#include <utility>

// finds the greatest common divisor of two integers
int gcd(int first, int second)
{
    // following algorithm assumes first is larger than the second
    if (first < second) std::swap(first, second);
    int remainder;
    do 
    {
        remainder = first % second;
        first = second;
        second = remainder;
    } while ( remainder != 0 );
    
    return first;
}
