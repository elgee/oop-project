/* Variable.h
 * holds the information about a variable. This object is stored in 
 * the variable table.
 * Members:
 * m_name a string which holds how the variable is referred to and is
 *  used as a key in the variable table
 * 
 * m_value a pointer to a Basic derived type which is the value or
 *  expression which the variable has been substituted for. The 
 *  default is nullptr which means there is no substitution.
 * 
 * Constructors:
 *  from a string to represent a name
 *  from a string and a value to be set m_value
 *  copy and move constructable
 * 
 * Member functions:
 * name() used to get and set name of variable
 * value() returns a const pointer to m_value
 * set(Basic const &) set m_value for substitution
 * reset() put m_value back to nullptr
 *
 * overloads:
 * operator>> inserts m_name into stream
 * 
 */
#ifndef VARIABLE_H
#define VARIABLE_H  

#include "Basic.h"
#include <memory>
#include <string>
#include <utility>
#include <iostream>

class Variable
{
public:
    Variable()
    {
    }
    Variable(std::string const &name) : m_name( name )
    {
    }
    Variable(std::string const &name, Basic const *value)
        : m_name( name )
    {
        m_value = value->copy();
    }
    Variable(Variable const &other) : m_name( other.m_name )
    {
        m_value = other.value()->copy();
    }
    Variable(Variable &&other) : Variable()
    {
        swap(*this, other);
    }
    Variable& operator=(Variable other)
    {
        swap(*this, other);
        return *this;
    }
    
    // setters and getters
    std::string name() const { return m_name; }
    void name(std::string const &n) { m_name = n; }
    std::unique_ptr<Basic> const&  value() const { return m_value; }
    // set or reset the value
    void set(Basic const &value) { m_value = value.copy(); }
    void reset() { m_value.reset(nullptr); }
    
    
    friend void swap(Variable &first, Variable &second)
    {
        std::swap(first.m_name, second.m_name);
        std::swap(first.m_value, second.m_value);
    }
    friend std::istream& operator>>(std::istream &is, Variable &variable);
private:
    std::string m_name;
    std::unique_ptr<Basic> m_value = nullptr;
};

#endif
