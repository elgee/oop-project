/* MathInt.cpp
 * Defines the operator+ and operator* and other member functions
 * Operations on MathInt return Basic pointers to a MathInt and 
 * operations acting on a MathRational types return Basic pointers
 * to MathRationals. 
 * Operations acting on types derived from the Expression class use
 * the implementation defined in the Expression class.
 */
#include "MathInt.h"
#include "MathRational.h"
#include "Sum.h"
#include "Product.h"

// call operator+(MathInt &) on the rhs
unique_ptr<Basic> MathInt::operator+(Basic const *rhs) const
{
    return *rhs + *this;
}

// calls operator*(MathInt &) on the rhs 
unique_ptr<Basic> MathInt::operator*(Basic const *rhs) const
{
    return *rhs * *this;
}

// Mathematical operator overloads
unique_ptr<Basic> MathInt::operator+(MathInt const &rhs) const
{
    unique_ptr<Basic> result{ new MathInt{ m_value + rhs.value() }};
    return result;
}

unique_ptr<Basic> MathInt::operator+(MathRational const &rhs) const
{
    unique_ptr<Basic> result{ new MathRational{ m_value +
        rhs.value() } };
    return result;
}

// operations on expression derived objects use expression 
// implementation
unique_ptr<Basic> MathInt::operator+(Sum const &rhs) const
{
    return rhs + *this;
}

unique_ptr<Basic> MathInt::operator+(Product const &rhs) const
{
    return rhs + *this;
}

unique_ptr<Basic> MathInt::operator+(VariablePtr const &rhs) const
{
    return rhs + *this;
}

unique_ptr<Basic> MathInt::operator+(Minus const &rhs) const
{
    return rhs + *this;
}

// multiplication by numberType objects
unique_ptr<Basic> MathInt::operator*(MathInt const &rhs) const
{
    unique_ptr<Basic> result{ new MathInt{ m_value * rhs.value() }};
    return result;
}

unique_ptr<Basic> MathInt::operator*(MathRational const &rhs) const
{
    unique_ptr<Basic> result{ new MathRational{ m_value *
        rhs.value() } };
    return result;
}

// multiplication by expressions
unique_ptr<Basic> MathInt::operator*(Sum const &rhs) const
{
    return rhs * *this;
}

unique_ptr<Basic> MathInt::operator*(Product const &rhs) const
{
    return rhs * *this;
}

unique_ptr<Basic> MathInt::operator*(Minus const &rhs) const
{
    return rhs * *this;
}

unique_ptr<Basic> MathInt::operator*(VariablePtr const &rhs) const
{
    return rhs * *this;
}

// returns a Basic pointer to a MathInt which holds the negative of
// the value held by *this
unique_ptr<Basic> MathInt::operator-() const
{
    return unique_ptr<Basic>{ new MathInt{ - m_value } };
}
