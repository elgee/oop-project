#include "Basic.h"

// calls the derived version of print 
std::ostream& operator<<(std::ostream &os, Basic const &basic)
{
    basic.print(os);
    return os;
}
