// A rational number class which stores the denominator and numerator as ints
// The class is move constructable and assignable.
// Mathematical operations have been overloaded and automatically simplify
// the fraction by dividing through by the greatest common denominator of 
// the numerator and denominator
// The numerator and denominator are stored as is and either one or both
// may be negative however when printing the sign of the rational number will
// be displayed first.
#ifndef RATIONAL_H
#define RATIONAL_H

#include <complex>
#include <iostream>

class Rational
{
public:
    // constructors and assignment
    Rational() : m_numerator{ 0 }, m_denominator{ 1 }
    {
    }
    Rational(int numerator, int denominator);
    Rational(Rational const &other) 
        : m_numerator{ other.m_numerator }, m_denominator{ other.m_denominator }
    {
    }
    Rational(Rational &&other); 
    Rational& operator=(Rational other);
    // getters and setters
    int numerator() { return m_numerator; }
    int denominator() { return m_denominator; }
    void numerator(int value) { m_numerator = value; }
    void denominator(int value) { m_denominator = value; }
    
    // unary operator
    Rational operator-() const;

    // mathematical overloads for rationals and ints
    Rational& operator+=(Rational const &rhs);
    Rational& operator+=(int const rhs);
    Rational& operator-=(Rational const &rhs);
    Rational& operator-=(int const rhs);
    Rational& operator*=(Rational const &rhs);
    Rational& operator*=(int const rhs);
    Rational& operator/=(Rational const &rhs);
    Rational& operator/=(int const rhs);
    
    // divide through by the greatest common factor. Returns this.
    Rational& simplify();
    
    // friend functions
    friend std::ostream& operator<<(std::ostream &os, Rational const &rational);
    friend std::istream& operator>>(std::istream &is, Rational &rational);
    friend void swap(Rational &first, Rational &second);
    
private:
    int m_numerator;
    int m_denominator;
};

// non member functions
Rational operator+(Rational lhs, Rational const &rhs);
Rational operator+(Rational lhs, int const rhs);
Rational operator+(int const lhs, Rational const &rhs);
Rational operator-(Rational lhs, Rational const &rhs);
Rational operator-(Rational lhs, int const rhs);
Rational operator-(int const lhs, Rational const &rhs);
Rational operator*(Rational lhs, Rational const &rhs);
Rational operator*(Rational lhs, int const rhs);
Rational operator*(int const lhs, Rational const &rhs);
Rational operator/(Rational lhs, Rational const &rhs);
Rational operator/(Rational lhs, int const rhs);
Rational operator/(int const lhs, Rational const &rhs);

// needed for simplify
int gcd(int first, int second);

// testing
void testRational();
#endif
