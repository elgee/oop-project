// Minus.cpp

#include "Minus.h"

#include <memory>
#include <forward_list>
#include <utility>

using std::unique_ptr;
using std::shared_ptr;
using std::forward_list;

unique_ptr<Basic> Minus::operator+(Basic const *rhs) const
{
    return *rhs + *this;
}

// implementation of operator+ for types apart from sum.
// creates a sum with this and the rhs as the terms
template <typename T> unique_ptr<Basic> Minus::add(T const &rhs) const
{
    forward_list<shared_ptr<Basic>> terms;
    terms.push_front(this->copy());
    Sum result{ terms };
    result += rhs;
    return unique_ptr<Basic>{ new Sum{ std::move(result) } };
}

// for sum no need to make new sum, just copy by value the sum passed
unique_ptr<Basic> Minus::operator+(Sum const &rhs) const
{
    Sum result{ rhs };
    result += *this;
    return unique_ptr<Basic>{ new Sum{ std::move(result) } };
}

unique_ptr<Basic> Minus::operator+(MathInt const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> Minus::operator+(MathRational const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> Minus::operator+(Product const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> Minus::operator+(Minus const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> Minus::operator+(VariablePtr const &rhs) const
{
    return this->add(rhs);
}
    
unique_ptr<Basic> Minus::operator*(Basic const *rhs) const
{
    return *rhs * *this;
}

// implementation of multiplication for types other than product
// where there is no need to make a new product and minus where 
// the term is returned. A product is created with this and the rhs
// as the terms
template <typename T> unique_ptr<Basic> Minus::times(T const &rhs) const
{
    unique_ptr<Basic> product{ *m_term * rhs };
    return unique_ptr<Basic>{ new Minus{ std::move(product) } };
}
    
unique_ptr<Basic> Minus::operator*(Product const &rhs) const
{
    Product product{ rhs };
    unique_ptr<Basic> result{ new Product{ std::move(product *= *this) } };
    return unique_ptr<Basic>{ new Minus{ std::move(result) } };
}

// return the product of the terms
unique_ptr<Basic> Minus::operator*(Minus const &rhs) const
{
    return m_term * rhs.m_term.get();
}

unique_ptr<Basic> Minus::operator*(MathInt const &rhs) const
{
    return this->times(rhs);
}
unique_ptr<Basic> Minus::operator*(MathRational const &rhs) const
{
    return this->times(rhs);
}
unique_ptr<Basic> Minus::operator*(Sum const &rhs) const
{
    return this->times(rhs);
}
unique_ptr<Basic> Minus::operator*(VariablePtr const &rhs) const
{
    return this->times(rhs);
}

// this returns the term that this is the negative of
unique_ptr<Basic> Minus::operator-() const
{
    return m_term->copy();
}

// For numeric types, the value is changed to be negative otherwise a 
// a minus object is returned. This implementation is defined in the 
// unitary operator-() which must be defined for Basic derived types.
unique_ptr<Basic> Minus::evaluate()
{
    return - *m_term->evaluate();
}

// if the precedence of the term which this is the negative is greater
// than the precedence of taking a negative then the result is enclosed
// in brackets.
void Minus::print(std::ostream &os) const
{
    os << "-";
    if (m_term->precedence() < m_precedence)
    {
        os << "(";
        m_term->print(os);
        os << ")";
    }
    else
        m_term->print(os);
}

unique_ptr<Basic> Minus::copy() const
{
    return unique_ptr<Basic>{ new Minus{ *this } };
}

void swap(Minus &first, Minus &second)
{
    std::swap(first.m_term, second.m_term);
    std::swap(first.m_order, second.m_order);
}
