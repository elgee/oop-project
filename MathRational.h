/* MathRational.h
 * Class derived from NumberType<Rational> which represents a rational
 * number and defines the operations with other types when accessed 
 * through a Basic pointer.
 */
#ifndef MATH_RATIONAL_H
#define MATH_RATIONAL_H

#include "Basic.h"
#include "Rational.h"
#include "MathInt.h"
#include "NumberType.h"
#include "Product.h"
#include "Sum.h"
#include "Minus.h"
#include <utility>
#include <memory>

class MathRational : public NumberType<Rational>
{
public:
	MathRational() : NumberType<Rational>()
    {
    }
    MathRational(Rational const &value) : NumberType<Rational>( value )
    {
    }
    MathRational(Rational &&value) : NumberType<Rational>( std::move(value) )
    {
    }
    MathRational(MathRational const &other) : NumberType<Rational>( other )
    {
    }
    MathRational(MathRational &&other) : NumberType<Rational>( std::move(other) )
    {
    }
    MathRational& operator=(MathRational other)
    {
        NumberType<Rational>::operator=(other);
        return *this;
    }
    
    // mathematical overloads
    std::unique_ptr<Basic> operator+(Basic const *rhs) const;
    std::unique_ptr<Basic> operator+(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator+(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator+(Sum const &rhs) const;
    std::unique_ptr<Basic> operator+(Product const &rhs) const;
    std::unique_ptr<Basic> operator+(Minus const &rhs) const;
    std::unique_ptr<Basic> operator+(VariablePtr const &rhs) const;
    
    std::unique_ptr<Basic> operator*(Basic const *rhs) const;
    std::unique_ptr<Basic> operator*(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator*(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator*(Sum const &rhs) const;
    std::unique_ptr<Basic> operator*(Product const &rhs) const;
    std::unique_ptr<Basic> operator*(Minus const &rhs) const;
    std::unique_ptr<Basic> operator*(VariablePtr const &rhs) const;

    // returns a pointer to another math rational with a negative
    // numerator
    std::unique_ptr<Basic> operator-() const;
    
    char const* type() const { return "Rational"; }
	std::unique_ptr<Basic> copy() const
	{
		return std::unique_ptr<Basic>{ new MathRational{ *this } };
	}
    // 	returns pointer to MathInt if denominator is 1
	std::unique_ptr<Basic> evaluate(); 
};

#endif
