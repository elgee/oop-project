/* Expression.h
 * Abstract class derived from Basic from which all expressions are 
 * derived.
 * Adds the protected member m_symbol which is a string to represent 
 * operation in output.
 * Constructors are defined for use in derived classes which can set 
 * the symbol and precedence of the Expression
 */
#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "Basic.h"
#include <string>
#include <utility>

class Minus;

class Expression : public Basic
{
public:
    Expression()
    {
    }
    Expression(std::string const &symbol, int const precedence) 
        : m_symbol( symbol )
    {
        m_precedence = precedence;
    }
    Expression(std::string &&symbol, int const precedence) 
        : m_symbol( std::move(symbol) )
    {
        m_precedence = precedence;
    }
    
protected:
	std::string m_symbol;
};

#endif
