cmake_minimum_required(VERSION 3.0)

project(project)

add_executable(project main.cpp Associative.cpp Basic.cpp Expression.cpp 
    functions.cpp MathInt.cpp MathRational.cpp Product.cpp Rational.cpp Sum.cpp 
    Variable.cpp VariablePtr.cpp VariableTable.cpp IO.cpp
    Minus.cpp)

install(TARGETS project RUNTIME DESTINATION bin)
