/* MathRational.cpp 
 * Mathematical operations are defined here. A pointer to a 
 * MathRational is returned when adding or multiplying by a MathInt
 * or MathRational. For Expression derived types, the implementation
 * defined in expression is used.
 * Evaluation overrides the NumberType<Rational> implementation to 
 * return a pointer to a MathInt if the denominator is 1.
 */
#include "Product.h"
#include "MathRational.h"
#include "MathInt.h"
#include "Sum.h"
#include "Product.h"

// Calls the Operator+(MathRational &) on the rhs
unique_ptr<Basic> MathRational::operator+(Basic const *rhs) const
{
    return *rhs + *this;
}

unique_ptr<Basic> MathRational::operator*(Basic const *rhs) const
{
    return *rhs * *this;
}

unique_ptr<Basic> MathRational::operator+(MathInt const &rhs) const
{
    Rational result = m_value + rhs.value();
    if (result.denominator() == 1)
        return unique_ptr<Basic>{ new MathInt{ result.numerator() } };
    else
        return unique_ptr<Basic>{ new MathRational{ std::move(result) } };
}

unique_ptr<Basic> MathRational::operator+(MathRational const &rhs) const
{
    Rational result = m_value + rhs.value();
    if (result.denominator() == 1)
        return unique_ptr<Basic>{ new MathInt{ result.numerator() } };
    else
        return unique_ptr<Basic>{ new MathRational{ std::move(result) } };
}

// Addition when adding to Expression derived classes use 
// implementation defined in that class
unique_ptr<Basic> MathRational::operator+(Sum const &rhs) const
{
    return rhs + *this;
}

unique_ptr<Basic> MathRational::operator+(Product const &rhs) const
{
    return rhs + *this;
}

unique_ptr<Basic> MathRational::operator+(Minus const &rhs) const
{
    return rhs + *this;
}

unique_ptr<Basic> MathRational::operator+(VariablePtr const &rhs) const
{
    return rhs + *this;
}

// Multiplication overloads 
unique_ptr<Basic> MathRational::operator*(MathInt const &rhs) const
{
    Rational result = m_value * rhs.value();
    if (result.denominator() == 1)
        return unique_ptr<Basic>{ new MathInt{ result.numerator() } };
    else
        return unique_ptr<Basic>{ new MathRational{ result } };
}

unique_ptr<Basic> MathRational::operator*(MathRational const &rhs) const
{
    Rational result = m_value * rhs.value();
    if (result.denominator() == 1)
        return unique_ptr<Basic>{ new MathInt{ result.numerator() } };
    else
        return unique_ptr<Basic>{ new MathRational{ std::move(result) } };
}

// Multiplication overloads on Expression derived types
unique_ptr<Basic> MathRational::operator*(Sum const &rhs) const
{
    return rhs * *this;
}

unique_ptr<Basic> MathRational::operator*(Product const &rhs) const
{
    return rhs * *this;
}

unique_ptr<Basic> MathRational::operator*(Minus const &rhs) const
{
    return rhs * *this;
}

unique_ptr<Basic> MathRational::operator*(VariablePtr const &rhs) const
{
    return rhs * *this;
}

// Returns a MathRational which holds a negative Rational
unique_ptr<Basic> MathRational::operator-() const
{
    return unique_ptr<Basic>{ new MathRational{ - m_value } };
}

// Returns a copy or Basic pointer to MathInt if denominator is 1
unique_ptr<Basic> MathRational::evaluate() 
{
    if (m_value.denominator() == 1)
        return unique_ptr<Basic>{ new MathInt{ m_value.numerator() } };
    else
        return copy();
}
