README.txt
Lewis Graham Jun 2019

This is a symbolic calculator that I worked on for a university course project. The goal was to create a calculator that could work with symbolic variables and simplify expressions, essentially a very simple computer algebra system.

It uses only the standard library and will compile using C++11 or later.

It only understands integers rational numbers and variables.
Use '/' to create a fraction eg. 3/5. Variables must only
contain the letters a-z or A-Z.
Evaluating an expression:
	Type in an expression and it will be evaluated.
	You can add (+), subtract (-) and multiply (*) as well as using 
    brackets e.g:

	[in] x + 3 * 2
	6 + x

Setting the value of a variable:
	The value of a variable may be set using =. The variable
	now has that value until it is set to another one or
	cleared with the clear command. e.g:

	[in] x = 2
	x = 2
	[in] x * 3
	6
	[in] clear x
	[in] x
	x

An expression can be evaluated with a variable set to a
given value on the same line using ','. e.g:

	[in] x * 3 / 2, x = 2
	3

Other commands:
	exit:	exit the program
	help:	show this message
