/* VariablePtr.cpp
 * Default implementations of multiplication and addition are defined
 * in private member functions. These use the operator+= and 
 * operator*= overloads. 
 * 
 * Multiplication: 
 *  this is implemented using the templated private member function
 * times which creates a Product with this and the rhs as terms. 
 * Multiplying by a product uses the implementation which is defined
 * in product instead.
 * 
 * Addition:
 *  this is implemented using the templated private memver function
 * add which creates a Sum with this and the rhs as terms. Adding a
 * Sum uses the implementation defined in Sum instead.
 *
 */
#include "VariablePtr.h"
#include <memory>
#include <string>
#include <exception>
#include <iostream>
#include <utility>

using std::unique_ptr;
using std::shared_ptr;
using std::forward_list;

// default constructor
VariablePtr::VariablePtr() : m_ptr{ nullptr }
{
    m_order = 1;
}

// copy constructor
VariablePtr::VariablePtr(VariablePtr const &other) 
    : m_ptr{ other.m_ptr }
{
    m_order = 1;
}

// move constructor
VariablePtr::VariablePtr(VariablePtr && other) : VariablePtr()
{
    swap(*this, other);
}

// setting directly from variable pointer
VariablePtr::VariablePtr(shared_ptr<Variable> const &ptr) : m_ptr{ ptr }
{
    if (ptr->value())
        m_order = ptr->value()->order();
    else
        m_order = 1;
}

void swap(VariablePtr &first, VariablePtr &second)
{
    std::swap(first.m_ptr, second.m_ptr);
}

// insert the held variable's name into os
void VariablePtr::print(std::ostream &os) const
{
    os << m_ptr->name();
}

unique_ptr<Basic> VariablePtr::copy() const
{
    return unique_ptr<Basic>{ new VariablePtr{ *this } };
}

// set the value of the variable that this manages and change the 
// order of this
void VariablePtr::set(Basic const *value)
{
    m_ptr->set(*value);
    m_order = value->order();
}

// if the variable has been set to a value return that otherwise
// return the variablePtr object. Variable:value() is nullptr when 
// not set
unique_ptr<Basic> VariablePtr::evaluate() 
{
    if (m_ptr->value())
        return m_ptr->value()->evaluate();
    else
        return copy();
}
unique_ptr<Basic> VariablePtr::evaluate()  const
{
    if (m_ptr->value())
        return m_ptr->value()->evaluate();
    else
        return copy();
}
unique_ptr<Basic> VariablePtr::operator+(Basic const *rhs) const
{
    return *rhs + *this;
}

unique_ptr<Basic> VariablePtr::operator*(Basic const *rhs) const
{
    return *rhs * *this;
}

// implementation of adding 
template <typename T>
unique_ptr<Basic> VariablePtr::add(T const &rhs) const
{
    forward_list<shared_ptr<Basic>> terms;
    // evaluate will return a new copy of this if variable not set 
    // and if set will return the current value of the variable
    terms.push_front(shared_ptr<Basic>{ evaluate() });
    Sum result{ terms };
    result += rhs;
    return unique_ptr<Basic>{ new Sum{ std::move(result) } };
}

// implementation of multiplying
template <typename T>
unique_ptr<Basic> VariablePtr::times(T const &rhs) const
{
    forward_list<shared_ptr<Basic>> terms;
    terms.push_front(shared_ptr<Basic>{ evaluate() });
    Product result{ terms };
    result *= rhs;
    return unique_ptr<Basic>{ new Product{ std::move(result) } };
}

// functions required by base
unique_ptr<Basic> VariablePtr::operator+(MathInt const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> VariablePtr::operator+(MathRational const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> VariablePtr::operator+(Product const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> VariablePtr::operator+(Minus const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> VariablePtr::operator+(Sum const &rhs) const
{
    return rhs + *this;
}
unique_ptr<Basic> VariablePtr::operator+(VariablePtr const &rhs) const
{
    return this->add(rhs);
}
unique_ptr<Basic> VariablePtr::operator*(MathInt const &rhs) const
{
    return this->times(rhs);
}
unique_ptr<Basic> VariablePtr::operator*(MathRational const &rhs) const
{
    return this->times(rhs);
}
unique_ptr<Basic> VariablePtr::operator*(Sum const &rhs) const
{
    return rhs * *this;
}
unique_ptr<Basic> VariablePtr::operator*(Product const &rhs) const
{
    return this->times(rhs);
}
unique_ptr<Basic> VariablePtr::operator*(Minus const &rhs) const
{
    return this->times(rhs);
}
unique_ptr<Basic> VariablePtr::operator*(VariablePtr const &rhs) const
{
    return this->times(rhs);
}

unique_ptr<Basic> VariablePtr::operator-() const
{
    return unique_ptr<Basic>{ new Minus{ this } };
}

