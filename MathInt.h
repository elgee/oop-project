/* MathInt.h
 * Class for representing an integer which is derived from 
 * NumberType<int> and overloads operator* and operator+ acting on 
 * other Basic derived types. 
 */
#ifndef MATH_INT_H
#define MATH_INT_H

#include "NumberType.h"
#include "MathRational.h"
#include "Sum.h"
#include "Product.h"
#include "Minus.h"
#include <utility>
#include <memory>

using std::unique_ptr;

class MathInt : public NumberType<int>
{
public:
    // constructors all use NumberType versions.
    MathInt() : NumberType<int>()
    {
    }
    MathInt(int const value) : NumberType<int>(value)
    {
    }
    MathInt(MathInt const &other) : NumberType<int>(other)
    {
    }
    MathInt(MathInt &&other) : NumberType<int>(std::move(other))
    {
    }
    MathInt& operator=(MathInt const &other)
    {
        NumberType<int>::operator=(other);
        return *this;
    }
    MathInt& operator=(MathInt &&other)
    {
        NumberType<int>::operator=(std::move(other));
        return *this;
    }
    
    // Mathematical overloads
    unique_ptr<Basic> operator+(Basic const *rhs) const;
    unique_ptr<Basic> operator+(MathInt const &rhs) const;
    unique_ptr<Basic> operator+(MathRational const &rhs) const;
    unique_ptr<Basic> operator+(Sum const &rhs) const;
    unique_ptr<Basic> operator+(Product const &rhs) const;
    unique_ptr<Basic> operator+(Minus const &rhs) const;
    unique_ptr<Basic> operator+(VariablePtr const &rhs) const;
    
    unique_ptr<Basic> operator*(Basic const *rhs) const;
    unique_ptr<Basic> operator*(MathInt const &rhs) const;
    unique_ptr<Basic> operator*(MathRational const &rhs) const;
    unique_ptr<Basic> operator*(Sum const &rhs) const;
    unique_ptr<Basic> operator*(Product const &rhs) const;
    unique_ptr<Basic> operator*(Minus const &rhs) const;
    unique_ptr<Basic> operator*(VariablePtr const &rhs) const;

    unique_ptr<Basic> operator-() const;
    
    char const* type() const { return "Int"; }
	unique_ptr<Basic> copy() const
	{
		return unique_ptr <Basic>{ new MathInt{ *this } };
	}
};

#endif 
