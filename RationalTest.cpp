#include <iostream>
#include "Rational.h"
#include <stdexcept>

void testRational()
{
    Rational a{ 1, 3 };
    Rational b{ 4, 6 };
   
    std::cout << "a = " << a << '\n';
    std::cout << "b = " << b << '\n';
    std::cout << "a * b = " << a * b << " = " << (a*b).simplify() << '\n';
    std::cout << "a / b = " << a / b << " = " << (a/b).simplify() << '\n';
    std::cout << "a + b = " << a + b << " = " << (a+b).simplify() << '\n';
    std::cout << "a - b = " << a - b << " = " << (a-b).simplify() << '\n';
    
    std::cout << "Enter a numerator: ";
    int numerator;
    std::cin >> numerator;
    std::cout << "Enter a denominator: ";
    int denominator;
    try
    {
        std::cin >> denominator;
        std::cout << Rational{ numerator, denominator }.simplify();
    }
    catch(std::runtime_error error)
    {
        std::cerr << error.what();
    }
}
