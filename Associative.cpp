// Associative.cpp

#include "Basic.h"
#include "Associative.h"
#include "Exceptions.h"
#include "Minus.h"

using std::shared_ptr; 
using std::forward_list;

Associative::Associative(std::string const &symbol, 
                         int const precedence,
                         forward_list<shared_ptr<Basic>> const &terms) 
    : Associative(symbol, precedence)
{
	if (!terms.empty())
	{
        // share ownership of items 
		for (auto it = terms.begin(); it != terms.end(); ++it)
		{
			shared_ptr<Basic> tmp{ *it };
			m_terms.push_front(tmp);
		}
		// this has reversed the order
		m_terms.reverse();
	}
}

Associative::Associative(std::string &&symbol, int const precedence,
                         forward_list<shared_ptr<Basic>> &&terms) 
    : Associative(symbol, precedence)
{
    std::swap(m_terms, terms);
}

// prints the list of terms with operators in between them, wrapping
// them in brackets if the precedence of the term is greater than 
// the precedence of the operation that this expression represents
void Associative::print(std::ostream& os) const
{
	// get length of m_terms to not print symbol after last element
    int size{ static_cast<int>(
                  std::distance(m_terms.begin(), m_terms.end())) };
    auto it = m_terms.begin();
    for (int element_count{ 1 }; element_count < size; ++element_count)
    {
        if ((*it)->precedence() < precedence())
            os << "(" << **it << ") ";
        else
            os << **it << " ";
        
        os << m_symbol << " ";
        ++it;
    }
    
    // don't dereference an empty list
    if (size != 0) os << **it;
}

// returns a Basic pointer to a Minus object which holds a copy
// of this.
unique_ptr<Basic> Associative::operator-() const
{
    return unique_ptr<Basic>{ new Minus{ copy() } };
}

// sorts terms according to their order
void Associative::sort()
{
    m_terms.sort([&] (shared_ptr<Basic> const &first, 
                      shared_ptr<Basic> const &second)
    {
        return first->order() < second->order();
    });
    m_sorted = true;
}
