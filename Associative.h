/* Associative.h
 * Abstract base class for holding associative expressions which is 
 * derived from Expression. Sum and Product are derived from this class
 * Constructors are provided which take the symbol and precedence as 
 * parameters for use in derived classes.
 * 
 * Members:
 *  m_terms is a forward list of shared Basic pointers which holds the 
 * terms in the operation
 *  m_sorted holds whether the terms are sorted (by order). this is 
 * checked before evaluation so that numeric terms are all together and
 * can be simplified
 * 
 * Methods:
 *  sort() sorts the terms based on their order
 *  print() provides a default implementation of printing which 
 * prints the list of terms with the symbol in between them enclosing
 * the term in brackets if the precedence is greater than that of the
 * operation that this represents.
 */

#ifndef ASSOCIATIVE_H
#define ASSOCIATIVE_H

#include "Basic.h"
#include "Expression.h"
#include <string>
#include <utility>
#include <iostream>
#include <forward_list>
#include <memory>

// abstract base class for associative operations
class Associative : public Expression
{
public:
    Associative() 
    {
    }
    Associative(std::string const &symbol, int const precedence) 
        : Expression(symbol, precedence)
    {
    }
    Associative(std::string &&symbol, int const precedence) 
        : Expression(std::move(symbol), precedence)
    {
    }
	Associative(std::string const &symbol, int const precedence,
                std::forward_list<std::shared_ptr<Basic>> const &terms);
	Associative(std::string &&symbol, int const precedence,
                std::forward_list<std::shared_ptr<Basic>> &&terms); 
    
    // copies this into a Minus
    std::unique_ptr<Basic> operator-() const;
    
    // sort the elements in terms of powers
    void sort();
    void print(std::ostream &os) const;
	    
protected:
    // terms on which the operation acts
	std::forward_list<std::shared_ptr<Basic>> m_terms;
    // keeps track of wether the list of terms is sorted 
    bool m_sorted = false;
            
};

#endif
