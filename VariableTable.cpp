// VariableTable.cpp


#include "VariableTable.h"
#include "Exceptions.h"
#include <utility>

// functions to add a variable to the map either by string or copying
// or moving it
void VariableTable::createVariable(std::string const &name)
{
    auto varPtr = std::make_shared<Variable>(name);
    m_table.insert(std::make_pair(name, varPtr));
}

void VariableTable::createVariable(Variable const &variable)
{
    auto varPtr = std::make_shared<Variable>(variable);
    m_table.insert(std::make_pair(variable.name(), varPtr));
}

void VariableTable::createVariable(Variable &&variable)
{
    auto varPtr = std::make_shared<Variable>(std::move(variable));
    m_table.insert(std::make_pair(variable.name(), varPtr));
}

// destroys variable but only if it is not being used by 
// something.
void VariableTable::destroyVariable(std::string const &name)
{
    if (m_table[name].unique()) 
        m_table.erase(name);
    else
        throw VariableInUse{};
};

// returns true if the table has a variable with name
bool VariableTable::exists(std::string const &name)
{
	if (m_table.count(name) == 0)
		return false;
	else
		return true;
}

// index the map for the variable with name, returning a shared_ptr to
// it 
std::shared_ptr<Variable> 
VariableTable::operator[](std::string const &name)
{
    if (m_table.count(name) == 1)
        return m_table[name];
    else
    {
        throw std::runtime_error{ "Error: Tried to index a variable that "
            "doesn't exist in the variable table"};
    }
}
            

    
