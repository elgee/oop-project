/* Basic.h
 * Abstract base class which forms an interface for mathematical 
 * operations between different numeric types, variables and
 * expressions.
 * 
 * Methods:
 *  Arithmetic + and * operators are overloaded with different types
 * as arguments to allow the evaluation of sums and products. Basic 
 * types may be on either side of the operation. The most derived
 * implementation of the lhs is called and then this calls the most
 * derived implementation of the operator on the lhs and so these
 * operations must be defined for all derived types.
 * 
 *  Unitary operator- used to implement minus signs and subtraction
 * 
 *  evaluate() returns the result of an expression or a copy of the 
 * term for non-expressions
 *  
 *  copy() returns a Basic pointer to a copy of the derived object
 * 
 *  print(std::ostream&) inserts the derived object into the passed 
 * output stream. This is called by the overloaded extraction operator.
 * 
 * Members:
 *  m_order is used for sorting by powers. The order is the sum of these
 * powers of variables
 * 
 *  m_precedence is used in printing to determine where brackets are
 * required. It gives the order that things are evaluated
 */


#ifndef BASIC_H
#define BASIC_H

#include <iostream>
#include <memory>

class MathInt;
class MathRational;
class Sum;
class Product;
class Minus;
class VariablePtr;


class Basic
{
public:
    // overloaded mathematical operators.
    virtual std::unique_ptr<Basic> operator+(Basic const *rhs) const = 0;
    virtual std::unique_ptr<Basic> operator+(MathInt const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator+(MathRational const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator+(Sum const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator+(Product const &rhs) const =0;
    virtual std::unique_ptr<Basic> operator+(Minus const &rhs) const =0;
    virtual std::unique_ptr<Basic> operator+(VariablePtr const &rhs) const =0;
    virtual std::unique_ptr<Basic> operator*(Basic const *rhs) const = 0;
    virtual std::unique_ptr<Basic> operator*(MathInt const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator*(MathRational const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator*(Sum const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator*(Product const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator*(Minus const &rhs) const = 0;
    virtual std::unique_ptr<Basic> operator*(VariablePtr const &rhs) const = 0;
    
    virtual std::unique_ptr<Basic> operator-() const = 0;
	virtual std::unique_ptr<Basic> evaluate() = 0;
    virtual void print(std::ostream &os) const = 0;
	virtual std::unique_ptr<Basic> copy() const = 0;
    friend std::ostream& operator<<(std::ostream &os, Basic const &basic);
    
    // returns a c-style string which is the type. This is only really 
    // used for checking that integers are input either side of a / and
    // could be removed once exponents have been implemented to have 
    // variable division.
    virtual char const* type() const = 0;
    
    // getters and setters
    // sum of powers of variables
    void order(int const order) { m_order = order; }
    int order() const { return m_order; }
    int& order() { return m_order; }
    // precedence in evaluation, used for inserting brackets in output
    int precedence() const { return m_precedence; }
    
protected:
    int m_order = 0;
    int m_precedence = 4;
};

/* these template functions allow the arithmetic operations to take a 
 * Basic pointer on the lhs which is useful for readability especially 
 * if operations are to be chained
 */
template<typename T>
std::unique_ptr<Basic> operator*(std::unique_ptr<Basic> const &lhs, 
                                 T const &rhs)
{
    return *lhs * rhs;
}

template<typename T>
std::unique_ptr<Basic> operator+(std::unique_ptr<Basic> const &lhs, 
                                 T const &rhs)
{
    return *lhs + rhs;
}
#endif
