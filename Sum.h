/* Sum.h
 * Class for holding a sum which is derived from Associative 
 * using "+" as the symbol. 
 * Precedence = 1
 * Order = 0
 * 
 * Constructors:
 *  Can be constructed from a list of terms
 *  copy and move constructable
 * 
 * Operations:
 *  operator* returns a Basic pointer to a Sum in which all the terms
 * have been multiplied by the rhs
 *  operator+ returns a Basic pointer to a Sum which has all the terms
 * in theis and the rhs.
 * 
 * default implementations of these are provided in private templated
 * functions which use the compound assignment operations.
 * 
 * evaluate() loops through the terms adding them together
 * through the use of the overloaded operator+ for Basic derived
 * types.
 */
#ifndef SUM_H
#define SUM_H

#include "Basic.h"
#include "Associative.h"
#include "Product.h"
#include "Minus.h"
#include "MathInt.h"
#include "MathRational.h"
#include "VariablePtr.h"
#include <forward_list>
#include <memory>
#include <iostream>
#include <utility>

class MathInt;
class MathRational;
class Product;
class Associative;

class Sum : public Associative
{
public:
    // constructors
    Sum() : Associative("+", 1)
    {
    }
    // construct by copying terms
    Sum(std::forward_list<std::shared_ptr<Basic>> const &terms);
    // construct by moving terms
    Sum(std::forward_list<std::shared_ptr<Basic>> &&terms);
    // copy constructor
    Sum(Sum const &other) ;
    // move constructor
    Sum(Sum &&other);
    // copy/move assignment
    Sum& operator=(Sum other);
    
    std::unique_ptr<Basic> evaluate();
    std::unique_ptr<Basic> copy() const;
    
    // combined assignment operations
    template <typename T> Sum& operator+=(T const &rhs);
    template <typename T> Sum& operator*=(T const &rhs);
    
    // overloads that return basic pointers
    std::unique_ptr<Basic> operator+(Basic const *rhs) const;
    std::unique_ptr<Basic> operator+(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator+(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator+(Sum const &rhs) const;
    std::unique_ptr<Basic> operator+(Product const &rhs) const;
    std::unique_ptr<Basic> operator+(Minus const &rhs) const;
    std::unique_ptr<Basic> operator+(VariablePtr const &rhs) const;
    std::unique_ptr<Basic> operator*(Basic const *rhs) const;
    std::unique_ptr<Basic> operator*(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator*(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator*(Sum const &rhs) const;
    std::unique_ptr<Basic> operator*(Product const &rhs) const;
    std::unique_ptr<Basic> operator*(Minus const &rhs) const;
    std::unique_ptr<Basic> operator*(VariablePtr const &rhs) const;
    
    char const* type() const { return "Sum"; }
    void print(std::ostream &os) const;
    friend void swap(Sum &first, Sum &second);
private:
    // default implementation of addition
    template <typename T> std::unique_ptr<Basic> add(T const &rhs) const;
    // default implementation of multiplying
    template <typename T> std::unique_ptr<Basic> times(T const &rhs) const;
};


#endif
