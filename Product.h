/* Product.h
 * Class for holding a product which is derived from Associative 
 * using "*" as the symbol. 
 * Precedence = 2
 * Order = sum of orders of terms 
 * 
 * Constructors:
 *  Can be constructed from a list of terms
 *  copy and move constructable
 * 
 * Operations:
 *  operator* returns a Basic pointer to a Product which has had the 
 * rhs appended to the list of terms and m_sorted set to false
 *  operator+ returns a Basic pointer to a Sum which has this and the 
 * rhs as the terms.
 * default implementations of these are provided in private templated
 * functions
 * 
 * evaluate() loops through the terms multiplying them together
 * through the use of the overloaded operator* for Basic derived
 * types.
 */
#ifndef PRODUCT_H
#define PRODUCT_H

#include "Basic.h"
#include "MathInt.h"
#include "MathRational.h"
#include "Associative.h"
#include "Sum.h"
#include "VariablePtr.h"
#include <forward_list>
#include <memory>

class Basic;
class Sum;

class Product : public Associative
{
public:
    
    // constructors
    Product();
    Product(std::forward_list<std::shared_ptr<Basic>> const &terms);
    Product(std::forward_list<std::shared_ptr<Basic>> &&terms);
    Product(Product const &other) ;
    Product(Product &&other);
    Product& operator=(Product other);
    
    // multiply the terms in turn
    std::unique_ptr<Basic> evaluate();
    
    // return a copy of this through a Basic pointer
    std::unique_ptr<Basic> copy() const;
    
    char const* type() const { return "Product"; }
    
    // used for implementing multiplication 
    template <typename T> Product& operator*=(T const &rhs);
    
    // math overloads required for a Basic derived type
    std::unique_ptr<Basic> operator+(Basic const *rhs) const;
    std::unique_ptr<Basic> operator+(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator+(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator+(Product const &rhs) const;
    std::unique_ptr<Basic> operator+(Sum const &rhs) const;
    std::unique_ptr<Basic> operator+(Minus const &rhs) const;
    std::unique_ptr<Basic> operator+(VariablePtr const &rhs) const;
    std::unique_ptr<Basic> operator*(Basic const *rhs) const;
    std::unique_ptr<Basic> operator*(MathInt const &rhs) const;
    std::unique_ptr<Basic> operator*(MathRational const &rhs) const;
    std::unique_ptr<Basic> operator*(Sum const &rhs) const;
    std::unique_ptr<Basic> operator*(Product const &rhs) const;
    std::unique_ptr<Basic> operator*(Minus const &rhs) const;
    std::unique_ptr<Basic> operator*(VariablePtr const &rhs) const;
    
    friend void swap(Product &first, Product &second);
private:
    // default implementation of adding Basic derived types
    template <typename T> 
    std::unique_ptr<Basic> add(T const &rhs) const;
    // default implementation of multiplying Basic derived types
    template <typename T> 
    std::unique_ptr<Basic> times(T const &rhs) const;
};

#endif
