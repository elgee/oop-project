/* NumberType.h
 * This is an abstract template class for types that represent
 * a numerical value such as integers, rational numbers, doubles, etc.
 * It has a protected member which is a copy of the number it 
 * represents.
 * Constructors are defined so that they can be used as the 
 * implementation of derived classes.
 * The print function is defined to print the value. 
 * 
 */
#ifndef NUMBER_TYPE_H
#define NUMBER_TYPE_H

#include "Basic.h"
#include <utility>
#include <iostream>
#include <exception>
#include <memory>

template<typename T>
class NumberType : public Basic
{
public:
    // constructors. Number types have the default order and precedence
    // of zero.
    NumberType() : m_value{}
    {
    }
    NumberType(T const &value) : m_value{ value }
    {
    }
    NumberType(T &&value) : m_value{ std::move(value) }
    {
    }
    NumberType(NumberType<T> const &other);
    NumberType(NumberType<T> &&other);
    NumberType<T>& operator=(NumberType<T> const &other);
    NumberType<T>& operator=(NumberType<T> &&other);
    
    // getters and setters
    T const& value() const { return m_value; }
    void value(T const &rhs) { m_value = rhs; }
    void value(T &&rhs) { m_value = std::move(rhs); }
    
    // evaluate returns a copy to end recursion 
    std::unique_ptr<Basic> evaluate() { return copy(); }
    // printing which is passed to << operator. Relies on T having << overload
    void print(std::ostream &os) const
    {
        os << m_value;
    }
    
    // non memeber functions
    friend void swap(NumberType<T> &first, NumberType<T> &second)
    {
        std::swap(first.m_value, second.m_value);
    }
protected:
    T m_value;
};

// copy and move constructors check for nullptr in other.m_value
template<typename T>
NumberType<T>::NumberType(NumberType<T> const &other) 
    : NumberType<T>(other.m_value)
{
}
template<typename T>
NumberType<T>::NumberType(NumberType<T> &&other) : NumberType<T>()
{
    swap(*this, other);
}
template<typename T>
NumberType<T>& NumberType<T>::operator=(NumberType<T> const &other)
{
    // deal with self assignment
    if (&other != this)
        return *this;
    
    m_value = other.m_value;
    
    return *this;
}
template<typename T>
NumberType<T>& NumberType<T>::operator=(NumberType<T> &&other)
{
    swap(*this, other);
    return *this;
}
#endif
