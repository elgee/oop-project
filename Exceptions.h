/* Exceptions.h
 * Exceptions specific to this program are derived here and inherit
 * from the runtime_error expression. This means they can all be 
 * caught with one catch in main.
 * 
 * NoTermsError: thrown when trying to do something with an expression
 * in which the list of terms is empty
 * 
 * VariableInUse: thrown when trying to delete a variable that has 
 * something pointing to it
 * 
 * BadInput: thrown by any of the functions in IO.h when they receive
 * something they don't expect. May be constructed with an error 
 * message so can be taylored to function.
 */
#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

class NoTermsError : public std::runtime_error
{
public:
	NoTermsError() 
		: std::runtime_error{ "Error: Attempt to use empty expression." }
	{
	}
};

class VariableInUse : public std::runtime_error
{
public:
    VariableInUse()
        : std::runtime_error{ "Error: Attempt to remove variable from "
                              "variable table whilst still in use." }
    {
    }
};

class BadInput : public std::runtime_error
{
public: 
    BadInput(char const *what_arg) : std::runtime_error{ what_arg }
    {
    }
};

#endif
